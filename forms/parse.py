from PyPDF2 import PdfReader
from sys import argv

class Form:
    def __init__(self,file):
        reader = PdfReader(file)
        #self.inputs = reader.get_form_text_fields()
        #self.boxes = reader.get_fields()
        self.data = reader.get_fields()
    def __getitem__(self, key):
        return self.data[key].get('/V','none')

        if key in self.inputs:
            return self.inputs[key]
        else:
            box = self.boxes[key]
            return box.get('/V','none')
    def keys(self):
        return self.data.keys()

        l = list(self.inputs.keys())+list(self.boxes.keys())
        return l

    def __repr__(self):
        return ', '.join( f'{k}: {self[k]}' for k in self.keys() )

def getDict(file):
    reader = PdfReader(file)
    inputs = reader.get_form_text_fields()
    boxes = reader.get_fields()
    return boxes

if __name__ == '__main__':
    candidats = []
    for f in argv[1:]:
        candidats.append(Form(f))
        #candidats.append(getDict(f))
    # to test    
    print(candidats)
