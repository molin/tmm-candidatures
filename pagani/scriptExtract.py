from PyPDF2 import PdfReader
from sys import argv


def getDict(file):
    reader = PdfReader(file)
    return reader.get_form_text_fields()

if __name__ == '__main__':
    candidats = []
    for f in argv[1:]:
        candidats.append(getDict(f))
    # to test    
    print(candidats)

    